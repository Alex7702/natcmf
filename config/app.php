<?php

return [
    /*
     * Общие настройки 
     */
    'siteDir' => dirname(__DIR__),
    'siteUrl' => 'http://natCMS.ru',
    /*
     * Настройки БД 
     */
    'dbParams' => [
        'dbHost' => 'localhost',
        'dbUser' => 'root',
        'dbPass' => '',
        'dbName' => 'natCMS',
        'dbPrefix' => 'nat_',
        'dbType' => 'sqlite3', //mysqli || pdo || sqllite3
        'dbType' => 'mysqli', //mysqli || pdo || sqllite3
        //Только для SQLite3
        'dbNameSqlite3' => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'sqllite3' . DIRECTORY_SEPARATOR . 'natDB', 
    ],
    /*
     * Доступные библиотеки для работы с БД
     */
    'possibleDb' => [
        'mysqli' => 'natCMF\core\mysql\DbMysqli',
        'sqlite3' => 'natCMF\core\mysql\DbSqlite3',
    ],
    
    //Дефолтный модуль, дефолтный контроллер имеет такое же имя как и модуль
    //дефолтный экшен всегда index
    'defaultModule' => 'front',
    
    //Меню в админке
    'adminNavBar' => [
        '/cabinet' => 'Главная',
        '/user/logout' => 'Выход',
    ],
];

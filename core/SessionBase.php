<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

/**
 * Description of SessionBase
 *
 * @author 27087
 */
interface SessionBase {
    
    /**
     * Старт сессии
     */
    public function start();
    
    /**
     * Взять значение сессии
     */
    public function get($key);
    
    /**
     * Установить значение сессии
     */
    public function set($key, $value);
    
    /*
     * Удалить из сессии
     */
    public function delete($key);
}

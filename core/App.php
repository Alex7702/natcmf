<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

use natCMF\core\Config;
use natCMF\core\Session;
use natCMF\core\Cookie;
use natCMF\core\Db;
use natCMF\core\SmartModel;
use natCMF\core\Cache;
use natCMF\app\models\User;

/**
 * Description of App
 *
 * @author 27087
 */
class App {

    /**
     * Создаем App и загружаем параметры приложения
     * @param array $settings
     */
    public function __construct(array $settings) {
        //Стартовали сессию
        Session::start();

        //Загрузили данные в конфиг
        foreach ($settings as $key => $value) {
            Config::set($key, $value);
        }
    }

    /**
     * Запускаем наше приложение
     */
    public function run() {
        /* Роутинг запускаем */
        $router = new Router(Config::get('defaultModule'));

//        if(Auth::isGuest() && Router::gets('0') == 'admin'){
//            self::error404();
//        }

        $router->run();
    }

    static function redirect($path, $params = []) {
        echo 'ридерект ' . $path . '<br />';
    }

    /**
     * Является ли запрос аякс запросом
     * @return boolean
     */
    static function isAjax() {
        $server = filter_input_array(INPUT_SERVER);
        if (isset($server['HTTP_X_REQUESTED_WITH']) && !empty($server['HTTP_X_REQUESTED_WITH']) && strtolower($server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Собираем ссылку
     * @param type $route
     * @param array $params массив с параметрами [1,2,3,4]
     * @return string ссылка на страницу проекта
     */
    static function href($route = '/', $params = []) {
        $url = '';

        /*
         * Текущая ссылка
         */
        if ($route == '') {
            return Config::get('siteUrl') . '/' . Router::uri();
        }

        if ($route == '/') {
            return Config::get('siteUrl');
        }

        if (strpos($route, '/') === 0) {
            $url .= $route;
        } else {
            $url .= '/' . Router::uri();
        }


        /*
         * Добавили параметры
         */
        if (!empty($params)) {
            foreach ($params as $param) {
                $url .= '/' . $param;
            }
        }
        return Config::get('siteUrl') . $url;
    }
    
}

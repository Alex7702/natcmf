<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

use Exception;

/**
 * Description of Db
 *
 * @author 27087
 */
class Db {

    /**
     * Храним экземпляр текущего класса
     * @var object Db 
     */
    static private $_instance = null;

    /**
     * Храним объекс работы с БД - имплементированный от интерфейса 
     * DbInterface
     * @var object - implement DbInterface. На данный момент это может быть
     * DbMysqli или DbSqlite3  
     */
    private $_db = null;

    /**
     * Создаем подключенте БД
     * @return object Db - возвращает экземпляр себя хранимый в _instance
     */
    static public function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;

            //Достали параметры подключения к БД
            $dbParams = Config::get('dbParams');
            
            //Возможные БД
            $possibleDb = Config::get('possibleDb');
            
            //Создаем объект
            $dbDriver = $possibleDb[$dbParams['dbType']]; 
            self::$_instance->_db = new $dbDriver($dbParams);

            
        }

        return self::$_instance;
    }

    /**
     * Запрос к БД
     * @param string $query
     * @param array $values
     * @return object 
     */
    static function query(string $query, array $values = []) {
        return self::getInstance()->_db->query($query, $values);
    }

    /**
     * Возвращает количество строк полученых в результате запроса к БД
     * @param type $result
     * @return int
     */
    static function numRows($result) {
        return self::getInstance()->_db->numRows($result);
    }

    /**
     * Очищает объект БД
     * @param type $result
     * @return boolean
     */
    static function freeResult(&$result) {
        return self::getInstance()->_db->freeResult($result);
    }

    /**
     * Перебирая объект $result возвращает строки в виде ассоциативного
     * массива
     * @param type $result
     * @return array
     */
    static function fetchAssoc($result) {
        return self::getInstance()->_db->fetchAssoc($result);
    }

    /**
     * Перебирая объект $result возвращает строки в виде индексированного
     * массива
     * @param type $result
     * @return array
     */
    static function fetchRow($result) {
        return self::getInstance()->_db->fetchRow($result);
    }

    /**
     * Получаем информацию о модели/таблице
     * @param type $table
     * @return array
     */
    static function tableInfo($table) {
        return self::getInstance()->_db->tableInfo($table);
    }

    /**
     * Возвращает количество запросов сделанных к базе
     * @return int
     */
    static function getCountQuery() {
        return self::getInstance()->_db->getCountQuery();
    }

}

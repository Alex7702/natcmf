<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core\mysql;

use natCMF\core\mysql\DbInterface;

/**
 * Description of DbSqlite3
 *
 * @author 27087
 */
class DbSqlite3 implements DbInterface {

    private $db = null;
    private $dbPrefix = null;
    private $colTypeLink = [
        'TEXT' => 'string',
        'INTEGER' => 'int',
        'FLOAT' => 'float',
    ];
    private $_count = 0;
    //Костыль для numRows
    private $numRows = 0;

    public function __construct($dbParams) {
        $this->dbPrefix = $dbParams['dbPrefix'];
        $this->db = new \SQLite3($dbParams['dbNameSqlite3']);
    }

    public function query(string $query, array $values = array()) {


        /* Типы плейсхолдеров */
        $pattern = '~{(int|float|string|array_int|array_string):(\S+?)}~';

        /* Выбираем из строки запрос возможные плейсходеры */
        $matches = [];
        preg_match_all($pattern, $query, $matches, PREG_SET_ORDER);

        /* Инициализируем массив с данными плейсхолтера */
        /* и массив с типами плейсхолдера mysqli 
         *   SQLITE3_INTEGER - соответствующая переменная имеет тип integer
         *   SQLITE3_FLOAT - соответствующая переменная имеет тип float
         *   SQLITE3_TEXT - соответствующая переменная имеет тип string
         *   SQLITE3_BLOB - соответствующая переменная является большим двоичным объектом (blob) и будет пересылаться пакетами
         */

        $placeHolders = [];


        /* Обходим найденные плейсхолдеры */
        foreach ($matches as $ph) {
            /* Временный плейсхоллер нужен для типов плейсхолдеров array_XXX 
              знак вопроса ? для самого запроса */
            $tmpPlaceholers = '';

            switch ($ph['1']) {
                case 'int':
                    $tmpPlaceholers = ':' . $ph['2'];
                    $placeHolders[] = [
                        'type' => 'SQLITE3_INTEGER',
                        'ph' => $tmpPlaceholers,
                        'value' => (int) $values[$ph['2']],
                    ];
                    break;

                case 'array_int':
                    $arrPlaceholers = '';
                    foreach ($values[$ph['2']] as $key => $intVal) {
                        $arrPlaceholers = ':' . $ph['2'] . '_' . $key;
                        $placeHolders[] = [
                            'type' => 'SQLITE3_INTEGER',
                            'ph' => $arrPlaceholers,
                            'value' => (int) $intVal,
                        ];
                    }
                    $tmpPlaceholers = $arrPlaceholers;
                    break;

                case 'string':
                    $tmpPlaceholers = ':' . $ph['2'];
                    $placeHolders[] = [
                        'type' => 'SQLITE3_TEXT',
                        'ph' => $tmpPlaceholers,
                        'value' => ':' . $ph['2'],
                    ];
                    break;

                case 'array_string':
                    break;

                default:

                    die('Неверный тип даннных ' . $ph['1']);
                    break;
            }
            /* Заменили плейсхолдер на нужное количество знаков вопросов */
            $query = str_replace($ph['0'], $tmpPlaceholers, $query);
        }
        /* Заменили плейсхолдер префикса таблицы на сам префикс */
        $query = str_replace('{db_prefix}', $this->dbPrefix, $query);

        //Костыль для num_rows
        $countQuery = '';
        if (preg_match('~SELECT([\s\S]{1,}?)FROM~m', $query)) {
            $countQuery = preg_replace('~SELECT([\s\S]{1,}?)FROM~m', 'SELECT COUNT(*) FROM', $query);
        }



        /* Готовим запрос и выполняем */
        if (!empty($placeHolders)) {

            //Костыль для num_rows
            if (!empty($countQuery)) {
                $stmt = $this->db->prepare($countQuery);
                foreach ($placeHolders as $v) {
                    $stmt->bindValue($v['ph'], $v['value'], constant($v['type']));
                }
                //Считаем запросы к БД
                $this->_count++;
                $result = $stmt->execute();
                $tmpCount = $this->fetchRow($result);
                $this->numRows = $tmpCount[0];
            }

            $stmt = $this->db->prepare($query);

            foreach ($placeHolders as $v) {
                $stmt->bindValue($v['ph'], $v['value'], constant($v['type']));
            }
            //Считаем запросы к БД
            $this->_count++;
            $result = $stmt->execute();
        } else {

            //Костыль для num_rows
            if (!empty($countQuery)) {
                //Считаем запросы к БД
                $this->_count++;
                $result = $this->db->query($countQuery);
                $tmpCount = $this->fetchRow($result);
                $this->numRows = $tmpCount[0];
            }
            //Считаем запросы к БД
            $this->_count++;
            $result = $this->db->query($query);
        }
        return $result;
    }

    public function fetchAssoc($result) {
        return $result->fetchArray(SQLITE3_ASSOC);
    }

    public function fetchRow($result) {
        return $result->fetchArray(SQLITE3_NUM);
    }

    public function freeResult(&$result) {
        return $result->finalize();
    }

    public function getCountQuery() {
        return $this->_count;
    }

    public function numRows($result) {
        $numRows = $this->numRows;
        $this->numRows = -1;
        return $numRows;
    }

    public function tableInfo($table) {

        $result = $this->query('PRAGMA table_info({db_prefix}' . $table . ')', []);

        $rows = [];
        while ($row = $this->fetchAssoc($result)) {
            $rows[$row['name']] = [
                'type' => $this->colTypeLink[$row['type']],
            ];
        }
        $this->freeResult($result);
        return $rows;
    }

}

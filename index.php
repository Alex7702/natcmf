<?php

$mem_start = memory_get_usage();

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/*
 * Дебаг информация
 */
ini_set('display_errors', 1);
error_reporting(E_ALL);

/* @var $settingsCore array */
$settingsApp = require __DIR__ . '/config/app.php';

/**
 * Автозагрузка классов приложения
 * @param type $classname
 */
function oneLoad($classname) {
    $path = str_replace('natCMF\\', '', $classname);
    $file = __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $path) . '.php';
    if (file_exists($file)) {
        include $file;
    }
}

spl_autoload_register('oneLoad');


//try {
//    test('gdf');
//} catch (Exception $e) {
//    echo $e->getMessage();
//}
//
//function test($n) {
//
//    if ($n > 0) {
//        throw new Exception('Больше нуля');
//    } else if ($n < 0) {
//        throw new Exception('Меньше нуля');
//    } else if ($n === 0) {
//        throw new Exception('Равно нулю');
//    } else {
//        throw new Exception('Непонятная хрень');
//    }
//}
//
//try {
//    test2('впав');
//} catch (Exception $e) {
//    echo $e->getMessage();
//}
//
//function test2($n) {
//
//    if ($n > 0) {
//        throw new AboveZeroException();
//    } else if ($n < 0) {
//        throw new LessThanZeroException();
//    } else if ($n === 0) {
//        throw new IsZeroException();
//    } else {
//        throw new IncomprehensibleСrapException();
//    }
//}
//
//class AboveZeroException extends Exception {
//
//    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
//        parent::__construct('Больше нуля', $code, $previous);
//    }
//
//}
//
//class LessThanZeroException extends Exception {
//
//    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
//        parent::__construct('Меньше нуля', $code, $previous);
//    }
//
//}
//
//class IsZeroException extends Exception {
//
//    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
//        parent::__construct('Равно нулю', $code, $previous);
//    }
//
//}
//
//class IncomprehensibleСrapException extends Exception {
//
//    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
//        parent::__construct('Непонятная хрень', $code, $previous);
//    }
//
//}
//
//
//die();

/*
 * Инициализируем приложение
 */
$app = new natCMF\core\App($settingsApp);
$app->run();



//Инфа о выполнении
echo '<hr style="margin-bottom: 0px;" />Запросов: ' . \natCMF\core\Db::getCountQuery() . '<br />Занято памяти: ' . round(((memory_get_usage() - $mem_start) / 1024 / 1024), 3) . ' Мб.';

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\app\controllers\cabinet;

use natCMF\core\ControllerBase;
use natCMF\core\View;
use natCMF\core\Config;

/**
 * Description of CabinetController
 *
 * @author 27087
 */
class CabinetController extends ControllerBase {

    public function actionIndex() {
        

        
        $view = new View('cabinet');
        $view->render('cabinet-index', [
            'pageId' => 'index',
            'adminMenuItems' => Config::get('adminNavBar'),
        ]);
        
    }

}

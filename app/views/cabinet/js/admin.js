$(function () {
    var imageType = /image.*/;

    //Слушаем формы загрузки изображений
    $('.image_download_form').on('change', function () {
        files = this.files;

        if (files[0].type.match(imageType)) {

            $(this).parent().find('div.image_preview').empty();
            var result_img = displayFiles(files[0], $(this).data('id'));
            if (result_img === true) {
                $(this).parent().find('input.image_download_btn').prop('disabled', false);
            }
        }
    }
    );


    //Слушаем кнопки удаления
    $('.image_delete_btn').on('click', function () {
        var $file_input = $(this).parent().find('input.image_link');
        var $div_img = $(this).parent().find('div.image_preview');
        var $img = $('<img>');
        var default_img = $(this).data('default');

        $.ajax({
            type: "POST",
            url: "/admin/options/commons",
            data: {delete: $file_input.val()},
            cache: false,
            success: function (responce) {
                $div_img.empty();
                $img.attr('src', default_img);
                $img.appendTo('#imagediv');
                $file_input.val(default_img);
            }
        });
    });


    // Отображение выбраных файлов и создание миниатюр
    function displayFiles(file, fileId) {

        // Отсеиваем не картинки
        if (!file.type.match(imageType)) {
            console.log('Файл отсеян: `' + file.name + '` (тип ' + file.type + ')');
            return false;
        }

        // Создаем элемент li и помещаем в него название, миниатюру и progress bar,
        // а также создаем ему свойство file, куда помещаем объект File (при загрузке понадобится)
        var preview = $(fileId);
        var img = $('<img/>').appendTo(preview);

        // Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
        // инфу обо всех файлах
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.attr('src', e.target.result);
                aImg.attr('width', 150);
                console.log('Картинка добавлена: ' + file.name + ' (' + Math.round(file.size / 1024) + ' Кб)');
            };
        })(img);

        reader.readAsDataURL(file);

        return true;

    }


    $('.delete_multi').on('click', function () {
        if (confirm("Удалить?")) {
            $(this).parent().parent().remove();
        }
    });


    $('.add_new_multi').on('click', function () {
        var $container = $(this).parent().find('.multi_new_container');

        $(this).parent().find('input, select, textarea').prop('disabled', function (el, oldVal) {
            if (oldVal) {
                $container.find('.dropdown-toggle').removeClass('disabled ');
                $container.find('.dropdown-toggle').attr('aria-disabled', false);
                $container.show();
                $('.add_new_multi').val('Отмена');

            } else {
                $container.find('.dropdown-toggle').addClass('disabled ');
                $container.find('.dropdown-toggle').attr('aria-disabled', true);
                $container.hide();
                $('.add_new_multi').val('Добавить новый');
            }
            return !oldVal;
        });
        $(this).prop('disabled', false);
    });



//    $('body').on('click', '.container_multi', function () {
    $(document).on('click', '.collapser', function () {
        console.log('11');
        var $container = $(this).parent().find('.multi_container');
//        if($container.css('display') === 'none'){
        if ($container.is(':hidden')) {
            $container.show();
        } else {
            $container.hide();

        }
        //$(this).parent().find('.container_multi').show();

    });



});

